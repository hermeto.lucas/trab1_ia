import pylint

# Classe Nó para o busca A*
class No():
    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position

# Funcao para achar o menor caminho no labirinto usando A* search
def astar(begin, end, maze, width, height, path):

    # Cria o no de começo e fim
    comeco_no = No(None, begin)
    comeco_no.g = comeco_no.h = comeco_no.f = 0
    fim_node = No(None, end)
    fim_node.g = fim_node.h = fim_node.f = 0

    maze[begin[0]][begin[1]] = '*'
    maze[end[0]][end[1]] = '*'

    # Inicializa a lista fechada e aberta
    aberta_list = []
    fechada_list = []

    # Coloca o no incial na lista aberta
    aberta_list.append(comeco_no)

    # Enquanto não encontrar o objetivo
    while len(aberta_list) > 0:

        # Pega o no atual da lista aberta
        no_atual = aberta_list[0]
        i_atual = 0
        for i, item in enumerate(aberta_list):
            if item.f < no_atual.f:
                no_atual = item
                i_atual = i

        # Pop o no atual e coloca ele na lista fechada
        aberta_list.pop(i_atual)
        fechada_list.append(no_atual)

        # Encontre o objetivo
        if no_atual == fim_node:
            path = []
            current = no_atual
            while current is not None:
                path.append(current.position)
                current = current.parent
            return path[::-1]

        # Gera filhos
        filhos = []
        for nova_pos in [(0, -1), (0, 1), (-1, 0), (1, 0), (-1, -1), (-1, 1), (1, -1), (1, 1)]:

            # Gera a posição do no
            no_pos = (no_atual.position[0] + nova_pos[0],
                      no_atual.position[1] + nova_pos[1])

            # Verifica se está no alcance
            if no_pos[0] > (len(maze) - 1) or no_pos[0] < 0 or no_pos[1] > (len(maze[len(maze)-1]) - 1) or no_pos[1] < 0:
                continue

            # Verifica se é possível passar por esse caminho
            if maze[no_pos[0]][no_pos[1]] != '*':
                continue

            # Cria um novo nó
            novo_no = No(no_atual, no_pos)

            if(novo_no in fechada_list):
                continue

            filhos.append(novo_no)

        # Para todos os filhos
        for filho in filhos:
            # Caso o filho esteja em uma lista fechada
            for closed_filho in fechada_list:
                if filho == closed_filho:
                    break
            else:
                # Cria as heuristicas para os filhos
                filho.g = no_atual.g + 1
                # H: Manhattan distance
                filho.h = abs(filho.position[0] - fim_node.position[0]) + abs(filho.position[1] - fim_node.position[1])
                filho.f = filho.g + filho.h

                # Caso o filho ja esteja na lista aberta
                for open_node in aberta_list:
                    # Verifica se o novo caminho é igual o pior
                    # do que algum no que esteja na lista aberta
                    if filho == open_node and filho.g >= open_node.g:
                        break
                else:
                    # Adiciona o filho na lista aberta
                    aberta_list.append(filho)

            
