import queue
import numpy as np

#distancia euclidiana
#def distance(p1, p2):
#    return np.sqrt(((p1[0]-p2[0])**2) + ((p1[1]-p2[1])**2))

#como a fila de prioridade não era foco do trabalho, a implementacao foi tirada de https://www.geeksforgeeks.org/priority-queue-in-python/
class PriorityQueue(object): 
    def __init__(self):
		
        self.queue = [] 
  
    def __str__(self): 
        return ' '.join([str(i) for i in self.queue]) 
  
    # for checking if the queue is empty 
    def isEmpty(self): 
        return len(self.queue) == [] 
  
    # for inserting an element in the queue 
    def insert(self, data): 
        self.queue.append(data) 
		
    # for popping an element based on Priority 
    def delete(self, end): 
        try: 
            min = 0
            for i in range(len(self.queue)): 
                if np.sqrt((((self.queue[i])[0]-end[0])**2) + (((self.queue[i])[1]-end[1])**2)) < np.sqrt((((self.queue[min])[0]-end[0])**2) + (((self.queue[min])[1]-end[1])**2)): 
                    min = i 
            item = self.queue[min] 
            del self.queue[min] 
            return item[0], item[1] 
        except IndexError: 
            print() 
            exit() 

# Função que encontra o menor caminho usando Best First
def bestfirst(lin, col, end, maze, width, height, _path):

    #inicia estruturas
    predecessor = np.zeros((height, width), dtype=(int, 2)) # matriz marcada inicialmente com 0s para depois ser preenchidas com os nos predecessores
    for i in range(height):
        for j in range(width):
            predecessor[i][j]=(-1,-1)

    q = PriorityQueue()
    #coloca noh inicial na fila
    q.insert((lin, col))

    invalid = (-1, -1)
    
    while q:
        #pega noh a ser tratado da fila
        lin, col = q.delete(end)

        #caso encontre o noh final, salva no caminho e para o algoritimo
        if(maze[lin][col] == '$'):
            _path.append([lin,col])
            break
        
        # Direita
        #se for um caminho valido e esse noh não tiver predecessor(não foi visitado)
        if(col + 1 < width and maze[lin][col+1] != '-' and
(predecessor[lin][col+1])[0] == -1 and maze[lin][col+1] != '#'):
            #print("Direita")
            predecessor[lin][col+1] = (lin, col) #salva o predecessor do noh que sera inserido na fila
            #insere novo noh na fila
            q.insert((lin, col+1))
        # Direita Baixo
        if(lin + 1 < height and col + 1 < width and maze[lin+1][col+1] != '-' and (predecessor[lin+1][col+1])[0] == -1 and maze[lin+1][col+1] != '#'):
            #print("Direita Baixo")
            predecessor[lin+1][col+1] = (lin, col)
            q.insert((lin+1, col+1))
        # Baixo
        if(lin + 1 < height and maze[lin+1][col] != '-' and (predecessor[lin+1][col])[0] == -1 and maze[lin+1][col] != '#'):
            #print("Baixo")
            predecessor[lin+1][col] = (lin, col)
            q.insert((lin+1, col))
        # Esquerda
        if(col - 1 >= 0 and maze[lin][col-1] != '-' and (predecessor[lin][col-1])[0] == -1 and maze[lin][col-1] != '#'):
            #print("Esquerda")
            predecessor[lin][col-1] = (lin, col)
            q.insert((lin, col-1))
        # Esquerda cima
        if(lin - 1 >= 0 and col - 1 >= 0 and maze[lin-1][col-1] != '-' and (predecessor[lin-1][col-1])[0] == -1 and maze[lin-1][col-1] != '#'):
            #print("Esquerda Cima")
            predecessor[lin-1][col-1] = (lin, col)
            q.insert((lin-1, col-1))
        # Cima
        if(lin - 1 >= 0 and maze[lin-1][col] != '-' and (predecessor[lin-1][col])[0] == -1 and maze[lin-1][col] != '#'):
            #print("Cima")
            predecessor[lin-1][col] = (lin, col)
            q.insert((lin-1, col))
        # Direita Cima
        if(lin - 1 >= 0 and col + 1 < width and maze[lin-1][col+1] != '-' and (predecessor[lin-1][col+1])[0] == -1 and maze[lin-1][col+1] != '#'):
            #print("Direita Cima")
            predecessor[lin-1][col+1] = (lin, col)
            q.insert((lin-1, col+1))
        # Baixo Esquerda
        if(lin + 1 < height and col - 1 >= 0 and maze[lin+1][col-1] != '-' and (predecessor[lin+1][col-1])[0] == -1 and maze[lin+1][col-1] != '#'):
            #print("Baixo Esquerdo")
            predecessor[lin+1][col-1] = (lin, col)
            q.insert((lin+1, col-1))

    #percorre pelos predecessores desde o noh final para encontrar o caminho
    while((predecessor[_path[-1][0]][_path[-1][1]])[0] != -1):
        _path.append(predecessor[_path[-1][0]][_path[-1][1]])

		
    #vira o vetor para o caminho não ficar ao contrário
    _path.reverse()

    return _path
