import queue
import numpy as np

# Função que encontra o menor caminho usando BFS
def bfs(lin, col, maze, width, height, _path):

    #inicia estruturas
    predecessor = np.zeros((height, width), dtype=(int, 2)) # matriz marcada inicialmente com 0s para depois ser preenchidas com os nos predecessores
    for i in range(height):
        for j in range(width):
            predecessor[i][j]=(-1,-1)

    q = queue.Queue()
    #coloca noh inicial na fila
    q.put(lin)
    q.put(col)
    
    while q:
        #pega noh a ser tratado da fila
        lin = q.get()
        col = q.get()

        #caso encontre o noh final, salva no caminho e para o algoritimo
        if(maze[lin][col] == '$'):
            _path.append([lin,col])
            break
        
        # Direita
        #se for um caminho valido e esse noh não tiver predecessor(não foi visitado)
        if(col + 1 < width and maze[lin][col+1] != '-' and (predecessor[lin][col+1])[0] == -1 and maze[lin][col+1] != '#'):
            #print("Direita")
            predecessor[lin][col+1] = (lin, col) #salva o predecessor do noh que sera inserido na fila
            #insere novo noh na fila
            q.put(lin)
            q.put(col+1)
        # Direita Baixo
        if(lin + 1 < height and col + 1 < width and maze[lin+1][col+1] != '-' and (predecessor[lin+1][col+1])[0] == -1 and maze[lin+1][col+1] != '#'):
            #print("Direita Baixo")
            predecessor[lin+1][col+1] = (lin, col)
            q.put(lin+1)
            q.put(col+1)
        # Baixo
        if(lin + 1 < height and maze[lin+1][col] != '-' and (predecessor[lin+1][col])[0] == -1 and maze[lin+1][col] != '#'):
            #print("Baixo")
            predecessor[lin+1][col] = (lin, col)
            q.put(lin+1)
            q.put(col)
        # Esquerda
        if(col - 1 >= 0 and maze[lin][col-1] != '-' and (predecessor[lin][col-1])[0] == -1 and maze[lin][col-1] != '#'):
            #print("Esquerda")
            predecessor[lin][col-1] = (lin, col)
            q.put(lin)
            q.put(col-1)
        # Esquerda cima
        if(lin - 1 >= 0 and col - 1 >= 0 and maze[lin-1][col-1] != '-' and (predecessor[lin-1][col-1])[0] == -1 and maze[lin-1][col-1] != '#'):
            #print("Esquerda Cima")
            predecessor[lin-1][col-1] = (lin, col)
            q.put(lin-1)
            q.put(col-1)
        # Cima
        if(lin - 1 >= 0 and maze[lin-1][col] != '-' and (predecessor[lin-1][col])[0] == -1 and maze[lin-1][col] != '#'):
            #print("Cima")
            predecessor[lin-1][col] = (lin, col)
            q.put(lin-1)
            q.put(col)
        # Direita Cima
        if(lin - 1 >= 0 and col + 1 < width and maze[lin-1][col+1] != '-' and (predecessor[lin-1][col+1])[0] == -1 and maze[lin-1][col+1] != '#'):
            #print("Direita Cima")
            predecessor[lin-1][col+1] = (lin, col)
            q.put(lin-1)
            q.put(col+1)
        # Baixo Esquerda
        if(lin + 1 < height and col - 1 >= 0 and maze[lin+1][col-1] != '-' and (predecessor[lin+1][col-1])[0] == -1 and maze[lin+1][col-1] != '#'):
            #print("Baixo Esquerdo")
            predecessor[lin+1][col-1] = (lin, col)
            q.put(lin+1)
            q.put(col-1)

    #percorre pelos predecessores desde o noh final para encontrar o caminho
    while((predecessor[_path[-1][0]][_path[-1][1]])[0] != -1):
        _path.append(predecessor[_path[-1][0]][_path[-1][1]])

		
    #vira o vetor para o caminho não ficar ao contrário
    _path.reverse()

    return _path
