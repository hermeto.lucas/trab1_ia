# Maze Solver

Coleção de Algoritmos de busca (duas cegas e duas informada)

## Algoritmos de busca
- Busca em Profundidade
- Busca em Largura
- Busca Best-First
- Busca A*

## Uso
```
Comando: python main.py <NomeDoArquivo> <busca>
onde <NomeDoArquivo> indica o arquivo que contém o labirinto no seguinte formato:
13 15
***#***********
***************
-------------**
***************
***************
***************
***************
**-************
**-************
**-****--------
**-************
**-****-****$**
**-****-*******
Onde: 
- A primeira linha representa as dimensões da matriz do tabuleiro
- O caracter '*' representa os espaços em branco
- O caracter '#' representa o ponto inicial do jogador
- O caracter '-' representa obstáculos
- O caracter '$' representa a posição do objetivo

e onde <busca> indica qual metodo de busca sera utilizado:
-d: DFS
-b: BFS
-f: Best-First Search
-a: A* Search
```

Ao finalizar o programa, será gerado um arquivo 'maze.png' com a imagem da solução do labirinto

# Versão

Utilizar a versão Python 3+ para executar o programa