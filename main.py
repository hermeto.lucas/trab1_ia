from sys import argv
import logging
import re
import plot
import dfs
import astar
import copy
import bfs
import BestFirst
import time

""" 
Funcao para realizar a leitura do arquivo
    Parametros:
        - path: caminho do arquivo com o labirinto
        - w: comprimento do labirinto
        - h: altura do labirinto
        - maze: matriz do labirinto
    Retorno:
        - 0: erro de arquivo nao encontrado
        - -1: rro na leitura do arquivo
        - w, h: retorna o comprimento e a altura caso concluido
"""
def file_open(path, w, h, maze):
    try:
        f = open(path, "r")
    except FileNotFoundError:
        return 0

    if f.mode == 'r':
        lines = f.readlines()
        for index, line in enumerate(lines):
            if index == 0:
                h, w = map(int, re.findall(r'\d+', line))
            else:
                line_list = list(line.translate({ord('\n'): None}))
                maze.append(line_list)
    else:
        return -1

    return w, h    


""" 
Funcao main que verifica os argumentos inseridos pelo usuario 
    e trata eles de acordo com a especificacao do programa
"""
def main():
    if len(argv) < 3:
        logging.error("Parametros incorretos")
        print("Para executar o programa digite: python3 main.py <NomeDoArquivo> <busca>")
        return 

    width, height, start_pos, end_pos = 0, 0, 0, 0
    maze = []

    f = file_open(argv[1], width, height, maze)
    if (f == 0):
        logging.error("Arquivo nao existe")
        return
    if (f == -1):
        logging.error("ERRO: Falha ao ler o arquivo")
        return
    else:
        width, height = f
        start_pos = [(index, row.index('#'))
            for index, row in enumerate(maze) if '#' in row]
        end_pos = [(index, row.index('$'))
            for index, row in enumerate(maze) if '$' in row]

    path = []
    temp_maze = copy.deepcopy(maze)

    if (argv[2] == '-d'): path = dfs.dfs(int(start_pos[0][0]), int(start_pos[0][1]), temp_maze, width, height, [])
    elif (argv[2] == '-a'): path = astar.astar(start_pos[0], end_pos[0], maze, width, height, [])
    elif (argv[2] == '-b'): path = bfs.bfs(int(start_pos[0][0]), int(start_pos[0][1]), maze, width, height, [])
    elif (argv[2] == '-f'): path = BestFirst.bestfirst(int(start_pos[0][0]), int(start_pos[0][1]), (end_pos[0][0], end_pos[0][1]), maze, width, height, [])
    else: logging.error("Falha na opcao escolhida")

    print(path)
    print(len(path))
    plot.save_png(maze, "maze.png", width, height, path)

main()
