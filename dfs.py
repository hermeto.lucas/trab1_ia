# Função que encontra o menor caminho usando DFS
achou = False

def dfs(lin, col, maze, width, height, _path):
    global achou
    _path.append([lin, col])
    if(maze[lin][col] == '$'):
        maze[lin][col] = '-'
        achou = True
        return _path
    maze[lin][col] = '-'

    # Direita
    if(col + 1 < width and maze[lin][col+1] != '-'):
        aux = dfs(lin, col+1, maze, width, height, _path)
        if(achou == True): return aux
    # Direita Baixo
    if(lin + 1 < height and col + 1 < width and maze[lin+1][col+1] != '-'):
        aux = dfs(lin+1, col+1, maze, width, height, _path)
        if(achou == True): return aux 
    # Baixo
    if(lin + 1 < height and maze[lin+1][col] != '-'):
        aux = dfs(lin+1, col, maze, width, height, _path)
        if(achou == True): return aux
    # Esquerda
    if(col - 1 >= 0 and maze[lin][col-1] != '-'):
        aux = dfs(lin, col-1, maze, width, height, _path)
        if(achou == True): return aux
    # Esquerda cima
    if(lin - 1 >= 0 and col - 1 >= 0 and maze[lin-1][col-1] != '-'):
        aux = dfs(lin-1, col-1, maze, width, height, _path)
        if(achou == True): return aux
    # Cima
    if(lin - 1 >= 0 and maze[lin-1][col] != '-'):
        aux = dfs(lin-1, col, maze, width, height, _path)
        if(achou == True): return aux
    # Direita Cima
    if(lin - 1 >= 0 and col + 1 < width and maze[lin-1][col+1] != '-'):
        aux = dfs(lin-1, col+1, maze, width, height, _path)
        if(achou == True): return aux
    # Baixo Esquerda
    if(lin + 1 < height and col - 1 >= 0 and maze[lin+1][col-1] != '-'):
        aux = dfs(lin+1, col-1, maze, width, height, _path)
        if(achou == True): return aux