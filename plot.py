import matplotlib.pyplot as plt
from matplotlib import collections as mc
import re

"""
Funcao para realizar o tratamento das linhas do labirinto
    Parametros:
        - maze: matriz do labirinto
        - width: comprimento do labirinto
        - height: altura do labirinto
    Retorno:
        Retorna linhas no formato do matplotlib na cor azul
"""
def png_output(maze, width, height):
    lines = [[(0, 0), (width, 0)], [(0, height), (width, height)],
             [(0, 0), (0, height)], [(width, 0), (width, height)]]
    for y in range(width):
        v = [[(y, x), (y, x + 1)]
             for x in range(height) 
                if ((x < height-1 and maze[x][y] == 0 and maze[x+1][y] == 0) 
                    or (x == height and maze[x-1][y] == 0))]
        h = [[(y, x), (y + 1, x)]
             for x in range(height) 
                if ((y < width-1 and maze[x][y] == 0 and maze[x][y+1] == 0)
                     or (y == width-1 and maze[x][y-1] == 0))]
        lines.extend(v)
        lines.extend(h)
    return mc.LineCollection(lines, colors="blue")


"""
Funcao para realizar o tratamento das linhas da solucao do labirinto
    Parametros:
        - maze: matriz do labirinto
        - path: caminho correto
    Retorno:
        Retorna linhas no formato do matplotlib na cor laranja
"""
def solution(maze, path):
    lines = []
    for i in range(len(path) - 1):
        y1 = path[i][0] + 0.5
        x1 = path[i][1] + 0.5
        y2 = path[i+1][0] + 0.5
        x2 = path[i+1][1] + 0.5
        lines.append([(x1, y1), (x2, y2)])
    return mc.LineCollection(lines, colors="orange")

"""
Funcao para salvar o arquivo .png
    Parametros:
        - lines: matriz com todas as linhas e formato delas
        - save: se ira salvar o arquivo (False como predefinido)
        - file: nome do arquivo que sera salvo (maze como predefinido)
"""
def plot(lines, save=False, file="maze"):
    fig, ax = plt.subplots()
    fig.suptitle('Maze Solver')
    plt.axis('off')
    plt.gca().invert_yaxis()
    for lc in lines:
        ax.add_collection(lc)
    ax.autoscale()

    if save:
        plt.savefig(file)
    else:
        plt.show()
    plt.close()

"""
Funcao para converter a matriz do labirinto e enviar ela para salvar o arquivo
    Parametros:
        - maze: matriz do labirinto
        - file: nome do arquivo que sera salvo
        - width: comprimento do labirinto
        - height: altura do labirinto
        - path: caminho correto
"""
def save_png(maze, file, width, height, path):
    temp_maze = maze
    for x in range(height):
        temp_maze[x] = [m.replace('*', '1') for m in temp_maze[x]]
        temp_maze[x] = [m.replace('#', '1') for m in temp_maze[x]]
        temp_maze[x] = [m.replace('$', '1') for m in temp_maze[x]]
        temp_maze[x] = [m.replace('-', '0') for m in temp_maze[x]]
        temp_maze[x] = list(map(int, temp_maze[x]))
    
    plot([png_output(temp_maze, width, height), solution(maze, path)], True, file)
